/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author ZQ215LE
 */
public class Configuration {

    public static Logger Logger;
    private static final String LOGGING_PATH = "log\\LoggingFile.log";

    public static void configLogger() {
        FileHandler fh;
        try {
            System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tF %1$tT %4$s %2$s %5$s%6$s%n");
            fh = new FileHandler(LOGGING_PATH, true);
            fh.setFormatter(new SimpleFormatter());
            Logger = Logger.getLogger("BankDemoLogger");
            Logger.setLevel(Level.FINER);
            Logger.addHandler(fh);
            Logger.setUseParentHandlers(false);

        } catch (IOException | SecurityException ex1) {
            Logger.log(Level.SEVERE, "Error message", ex1);
        }
    }

}
