/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author ZQ215LE
 */
public class Messages {

    public static final String APPLICATION_STARTED_MESSAGE = ""
            + "\n"
            + ""
            + " ____    _    _   _ _  __  ____  _____ __  __  ___  \n"
            + "| __ )  / \\  | \\ | | |/ / |  _ \\| ____|  \\/  |/ _ \\ \n"
            + "|  _ \\ / _ \\ |  \\| | ' /  | | | |  _| | |\\/| | | | |\n"
            + "| |_) / ___ \\| |\\  | . \\  | |_| | |___| |  | | |_| |\n"
            + "|____/_/   \\_\\_| \\_|_|\\_\\ |____/|_____|_|  |_|\\___/ \n"
            + "                                                    "
            + ""
            + "";
    public static final String TRANSACTION_ERROR_MESSAGE = "Error! Failed to execute database transaction!";
    public static final String SQL_ERROR_MESSAGE = "Error! Failed to execute SQL statement!";
    public static final String RESULT_SET_ERROR_MESSAGE = "Error while closing the Result Set object!";
    public static final String STATEMENT_ERROR_MESSAGE = "Error while closing the Statement object!";
    public static final String CONNECTION_ERROR_MESSAGE = "Database connection error!";
    public static final String CONNECTION_CREDENTIALS_ERROR_MESSAGE = "Error! Bad database connection credentials!";
    public static final String PATH_PARAMETERS_ERROR_MESSAGE = "Incorrect path parameters!";
    public static final String ARGUMENTS_ERROR_MESSAGE = "Incorrect or missing method arguments! The required arguments to run the application are fileName1.csv and fileName2.csv ";
    public static final String ACCOUNTS_LINE_WARNING_MESSAGE = "Invalid line found in accounts.csv!";
    public static final String TRANSACTIONS_LINE_WARNING_MESSAGE = "Invalid line found in transactions.csv!";
    public static final String ACCOUNT_INSERTED_INFO_MESSAGE = "An Account has been inserted.";
    public static final String TRANSACTION_INSERTED_INFO_MESSAGE = "A Transaction has been inserted.";
    public static final String TRANSFER_SUCCESSFUL_INFO_MESSAGE = "Transfer successful!";
    public static final String TRANSFER_UNSUCCESSFUL_INFO_MESSAGE = "Transfer unsuccessful! Insufficent funds!";
}
