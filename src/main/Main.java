/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import controller.Controller;
import form.BankDemoFrm;
import java.io.IOException;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import util.Configuration;
import util.Messages;
import static util.Configuration.Logger;

/**
 *
 * @author ZQ215LE
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    private static final String ACCOUNTS_PATH = "resource\\accounts.csv";
    private static final String TRANSACTIONS_PATH = "resource\\transactions.csv";
    
    public static void main(String[] args) {
        
        Configuration.configLogger();
        
        Configuration.Logger.log(Level.INFO, Messages.APPLICATION_STARTED_MESSAGE);
        
        boolean areThereNoArgs = args.length == 0;
        
        try {
            if (areThereNoArgs) {
                Controller.getInstance().readCSVFilesAndInsertDataIntoDB(ACCOUNTS_PATH, TRANSACTIONS_PATH);
            } else {
                Controller.getInstance().readCSVFilesAndInsertDataIntoDB(args[0], args[1]);
            }
            
            openForm();
            
        } catch (IOException ex) {
            openErrorForm(Messages.PATH_PARAMETERS_ERROR_MESSAGE);
        } catch (ArrayIndexOutOfBoundsException ex) {
            openErrorForm(Messages.ARGUMENTS_ERROR_MESSAGE);
        } catch (NullPointerException ex) {
            openErrorForm(Messages.CONNECTION_CREDENTIALS_ERROR_MESSAGE);
        }
    }
    
    private static void openForm() {
        BankDemoFrm frm = new BankDemoFrm();
        frm.setVisible(true);
        frm.setLocationRelativeTo(null);
    }
    
    private static void openErrorForm(String errorMessage) {
        Logger.log(Level.SEVERE, errorMessage);
        JOptionPane.showMessageDialog(null, errorMessage);
    }
    
}
