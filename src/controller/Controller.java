/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.DBBroker;
import domain.Account;
import domain.Transaction;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.Configuration;
import util.Messages;
import util.Status;

/**
 *
 * @author ZQ215LE
 */
public class Controller {

    private static final Logger LOGGER = Configuration.Logger;
    private static final String SPLIT_BY = "\\s*,\\s*";

    private static Controller instance;
    private DBBroker dbb;

    public Controller() {
        dbb = new DBBroker();
    }

    public static Controller getInstance() {
        if (instance == null) {
            instance = new Controller();
        }
        return instance;
    }

    public List<Account> getAllAccounts() {
        LOGGER.entering(Controller.class.getName(), "getAllAccounts");
        dbb.openConnection();
        List<Account> allAccounts = null;
        try {
            allAccounts = dbb.getAllAccounts();
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, Messages.TRANSACTION_ERROR_MESSAGE, ex);
        } finally {
            dbb.closeConnection();
        }

        LOGGER.exiting(Controller.class.getName(), "getAllAccounts", allAccounts);
        return allAccounts;
    }

    public void readCSVFilesAndInsertDataIntoDB(String csvAccounts, String csvTransactions) throws IOException, NullPointerException {
        LOGGER.entering(Controller.class.getName(), "readCSVFilesAndInsertDataIntoDB", new String[]{"csvAccounts", "csvTransactions"});
        emptyAllTablesFromDB();

        List<Account> accounts = readAccountsFromCSV(csvAccounts);
        insertAccount(accounts);

        List<Transaction> transactions = readTransactionsFromCSV(csvTransactions);
        executeTransfer(transactions);
        LOGGER.exiting(Controller.class.getName(), "readCSVFilesAndInsertDataIntoDB");
    }

    private void emptyAllTablesFromDB() {
        dbb.openConnection();
        try {
            dbb.emptyTables();
            dbb.commit();
        } catch (SQLException ex) {
            dbb.rollback();
            LOGGER.log(Level.SEVERE, Messages.TRANSACTION_ERROR_MESSAGE, ex);
        } finally {
            dbb.closeConnection();
        }
    }

    private List<Account> readAccountsFromCSV(String csvAccounts) throws IOException {
        List<Account> accounts = new ArrayList<>();
        List<String> lines = readLinesFromCSV(csvAccounts);
        for (String line : lines) {
            Account acc = readAccountFromTheLine(line);
            if (acc != null) {
                accounts.add(acc);
            }
        }
        return accounts;
    }

    private List<Transaction> readTransactionsFromCSV(String csvTransactions) throws IOException {
        List<Transaction> transactions = new ArrayList<>();
        List<String> lines = readLinesFromCSV(csvTransactions);
        for (String line : lines) {
            Transaction transaction = readTransactionFromTheLine(line);
            if (transaction != null) {
                transactions.add(transaction);
            }
        }
        return transactions;
    }

    private List<String> readLinesFromCSV(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        List<String> lines = new ArrayList<>();
        String line;
        while ((line = br.readLine()) != null) {
            lines.add(line);
        }
        return lines;
    }

    private Account readAccountFromTheLine(String line) throws IOException {
        Account account = new Account();
        String[] wordsInALine = line.split(SPLIT_BY);
        if (!validateAccountsCSVLine(wordsInALine)) {
            LOGGER.log(Level.WARNING, Messages.ACCOUNTS_LINE_WARNING_MESSAGE);
            return null;
        }
        try {
            for (int i = 0; i < wordsInALine.length; i = i + 2) {
                String name = wordsInALine[i];
                double balance = Double.valueOf(wordsInALine[i + 1]);
                account.setBalance(balance);
                account.setName(name);
            }
        } catch (NumberFormatException ex) {
            LOGGER.log(Level.SEVERE, Messages.PATH_PARAMETERS_ERROR_MESSAGE, ex);
            throw new IOException();
        }

        return account;
    }

    private boolean validateAccountsCSVLine(String[] wordsInALine) {
        return wordsInALine.length == 2;
    }

    private Transaction readTransactionFromTheLine(String line) throws IOException {
        Transaction transaction = new Transaction();
        String[] wordsInALine = line.split(SPLIT_BY);
        try {
            if (!validateTransactionsCSVLine(wordsInALine)) {
                LOGGER.log(Level.WARNING, Messages.TRANSACTIONS_LINE_WARNING_MESSAGE);
                return null;
            }
            for (int i = 0; i < wordsInALine.length; i = i + 3) {
                String from = wordsInALine[i];
                String to = wordsInALine[i + 1];
                double amount = Double.valueOf(wordsInALine[i + 2]);
                transaction.setAccountFrom(getAccountByName(from));
                transaction.setAccountTo(getAccountByName(to));
                transaction.setAmount(amount);
                transaction.setStatus(0);
            }
        } catch (NumberFormatException ex) {
            LOGGER.log(Level.SEVERE, Messages.PATH_PARAMETERS_ERROR_MESSAGE, ex);
            throw new IOException();
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, Messages.SQL_ERROR_MESSAGE, ex);
        }
        return transaction;
    }

    private boolean validateTransactionsCSVLine(String[] wordsInALine) throws SQLException {
        return (wordsInALine.length == 3 && validateAccountsInDB(wordsInALine));
    }

    private boolean validateAccountsInDB(String[] wordsInALine) throws SQLException {
        String accountFromName = wordsInALine[0];
        String accountToName = wordsInALine[1];
        boolean accountFromExists = getAccountByName(accountFromName).getName() != null;
        boolean accountToExists = getAccountByName(accountToName).getName() != null;
        return (accountFromExists && accountToExists);
    }

    private Account getAccountByName(String accountName) throws SQLException {
        dbb.openConnection();
        Account foundAccount = dbb.getAccountByName(accountName);
        dbb.closeConnection();
        return foundAccount;
    }

    private void insertAccount(List<Account> accounts) {
        for (Account acc : accounts) {
            insertAccount(acc);
        }
    }

    private void insertAccount(Account acc) {
        dbb.openConnection();
        try {
            dbb.insertAccount(acc);
            dbb.commit();
            LOGGER.log(Level.INFO, MessageFormat.format(" {0} [Account | name: {1}]", Messages.ACCOUNT_INSERTED_INFO_MESSAGE, acc.getName()));
        } catch (SQLException ex) {
            dbb.rollback();
            LOGGER.log(Level.SEVERE, Messages.TRANSACTION_ERROR_MESSAGE, ex);
        } finally {
            dbb.closeConnection();
        }
    }

    private void executeTransfer(List<Transaction> transactions) {
        for (Transaction transaction : transactions) {
            executeTransfer(transaction);
        }
    }

    public Transaction executeTransfer(Transaction transaction) {
        dbb.openConnection();
        try {
            if (isThereEnoughFundsToTransfer(transaction)) {
                transaction.setStatus(Status.SUCCESSFUL);
                dbb.executeTransfer(transaction);
            } else {
                transaction.setStatus(Status.UNSUCCESSFUL);
                dbb.insertTransaction(transaction);
            }
            dbb.commit();
            LOGGER.log(Level.INFO, MessageFormat.format(" {0} [Transaction | amount: {1} | status : {2}]", Messages.TRANSACTION_INSERTED_INFO_MESSAGE, transaction.getAmount(), transaction.getStatus()));
        } catch (SQLException ex) {
            dbb.rollback();
            transaction.setStatus(Status.ERROR);
            LOGGER.log(Level.SEVERE, Messages.TRANSACTION_ERROR_MESSAGE, ex);
        } finally {
            dbb.closeConnection();
        }
        return transaction;
    }

    private boolean isThereEnoughFundsToTransfer(Transaction transaction) throws SQLException {
        Account accountFrom = dbb.getAccount(transaction.getAccountFrom());
        return (accountFrom.getBalance() - transaction.getAmount() >= 0) && accountFrom.getBalance() > 0;
    }

}
