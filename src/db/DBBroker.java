/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import domain.Account;
import domain.Transaction;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import util.Configuration;
import util.Messages;

/**
 *
 * @author ZQ215LE
 */
public class DBBroker {

    private Connection connection;

    private static final Logger LOGGER = Configuration.Logger;

    public void openConnection() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/bank", "root", "mySqlServer");
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, Messages.CONNECTION_ERROR_MESSAGE, ex);
        }
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, Messages.CONNECTION_ERROR_MESSAGE, ex);
        }
    }

    public void commit() {
        try {
            connection.commit();
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, Messages.CONNECTION_ERROR_MESSAGE, ex);
        }
    }

    public void rollback() {
        try {
            connection.rollback();
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, Messages.CONNECTION_ERROR_MESSAGE, ex);
        }
    }

    public List<Account> getAllAccounts() throws SQLException {
        List<Account> allAccounts = new ArrayList<>();
        String sql = "SELECT * FROM ACCOUNT";
        Statement s = null;
        ResultSet rs = null;
        try {
            s = connection.createStatement();
            rs = s.executeQuery(sql);
            while (rs.next()) {
                int accountID = rs.getInt("ID");
                String name = rs.getString("NAME");
                double balance = rs.getDouble("BALANCE");
                Account acc = new Account(accountID, name, balance);
                allAccounts.add(acc);
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, Messages.SQL_ERROR_MESSAGE, ex);
            throw new SQLException();
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, Messages.STATEMENT_ERROR_MESSAGE, ex);
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, Messages.RESULT_SET_ERROR_MESSAGE, ex);
                }
            }
        }
        return allAccounts;
    }

    public Account getAccount(Account account) throws SQLException {
        String sql = "SELECT * FROM ACCOUNT WHERE ID = ?";
        Account foundAccount = new Account();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, account.getAccountID());
            rs = ps.executeQuery();
            while (rs.next()) {
                foundAccount.setAccountID(rs.getInt("ID"));
                foundAccount.setName(rs.getString("NAME"));
                foundAccount.setBalance(rs.getDouble("BALANCE"));
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, Messages.SQL_ERROR_MESSAGE, ex);
            throw new SQLException();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, Messages.STATEMENT_ERROR_MESSAGE, ex);
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, Messages.RESULT_SET_ERROR_MESSAGE, ex);
                }
            }
        }
        return foundAccount;
    }

    public void executeTransfer(Transaction transaction) throws SQLException {
        insertTransaction(transaction);
        updateAccountFrom(transaction);
        updateAccountTo(transaction);
    }

    public void insertTransaction(Transaction transaction) throws SQLException {
        String sql = "INSERT INTO TRANSACTION (FROM_ID,TO_ID,AMOUNT,STATUS) VALUES (?,?,?,?)";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, transaction.getAccountFrom().getAccountID());
            ps.setInt(2, transaction.getAccountTo().getAccountID());
            ps.setDouble(3, transaction.getAmount());
            ps.setInt(4, transaction.getStatus());
            ps.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, Messages.SQL_ERROR_MESSAGE, ex);
            throw new SQLException();
        }
    }

    private void updateAccountFrom(Transaction transaction) throws SQLException {
        String sql = "UPDATE ACCOUNT SET BALANCE = BALANCE - ? WHERE ID = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setDouble(1, transaction.getAmount());
            ps.setDouble(2, transaction.getAccountFrom().getAccountID());
            ps.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, Messages.SQL_ERROR_MESSAGE, ex);
            throw new SQLException();
        }
    }

    private void updateAccountTo(Transaction transaction) throws SQLException {
        String sql = "UPDATE ACCOUNT SET BALANCE = BALANCE + ? WHERE ID = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setDouble(1, transaction.getAmount());
            ps.setDouble(2, transaction.getAccountTo().getAccountID());
            ps.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, Messages.SQL_ERROR_MESSAGE, ex);
            throw new SQLException();
        }
    }

    public void insertAccount(Account acc) throws SQLException {
        String sql = "INSERT INTO ACCOUNT (NAME, BALANCE) VALUES (?,?)";;
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, acc.getName());
            ps.setDouble(2, acc.getBalance());
            ps.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, Messages.SQL_ERROR_MESSAGE, ex);
            throw new SQLException();
        }
    }

    public void emptyTables() throws SQLException {
        String sql1 = "DELETE FROM ACCOUNT";
        String sql2 = "DELETE FROM TRANSACTION";
        PreparedStatement ps = connection.prepareStatement(sql2);
        ps.executeUpdate();
        ps = connection.prepareStatement(sql1);
        ps.executeUpdate();
        ps.close();
    }

    public Account getAccountByName(String accountName) throws SQLException {
        String sql = "SELECT * FROM ACCOUNT WHERE NAME = ? ";
        Account foundAccount = new Account();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, accountName);
            rs = ps.executeQuery();
            while (rs.next()) {
                foundAccount.setAccountID(rs.getInt("ID"));
                foundAccount.setName(rs.getString("NAME"));
                foundAccount.setBalance(rs.getDouble("BALANCE"));
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.SEVERE, Messages.SQL_ERROR_MESSAGE, ex);
            throw new SQLException();
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, Messages.STATEMENT_ERROR_MESSAGE, ex);
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOGGER.log(Level.SEVERE, Messages.RESULT_SET_ERROR_MESSAGE, ex);
                }
            }
        }
        return foundAccount;
    }
}
